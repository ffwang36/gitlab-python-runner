"""
Constants for gitlab-python-runner
"""
NAME = "gitlab-python-runner"
VERSION = "12.3.0-1"
USER_AGENT = "{} {}".format(NAME, VERSION)
